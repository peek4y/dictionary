'use strict';
const BASE_URL = 'http://api.wordnik.com:80/v4';
const fetch = require('node-fetch');
const API_KEY = '1ca5a6091b594fad9820504d93f0dc598ebb798632339b3ce';
const Table = require('cli-table');

class WordFetchApi {
    constructor() {
        let _self = this;
        _self.wordUrl = `${BASE_URL}/word.json`;
        _self.wordsUrl = `${BASE_URL}/words.json`;
    }
    fetch(url, query) {
        let _self = this;
        query = query instanceof Object ? (() => {
            query['api_key'] = API_KEY;
            return query;
        })() : { api_key: API_KEY };
        let iterator = Object.keys(query);
        let isFirst = true;
        let params = '';
        iterator.forEach(key => {
            if (isFirst) {
                params += `?${key}=${query[key]}`;
                isFirst = false;
            } else {
                params += `&${key}=${query[key]}`;
            }
        });
        url = `${url}${params}`;
        return fetch(url).then(_self._result);
    }
    _result(res) {
        return res.json();
    }
    definition(word) {
        if (!word) {
            return Promise.reject(`Invalid word - ${word}`);
        }
        let _self = this;
        return _self.fetch(`${_self.wordUrl}/${word}/definitions`, {
            partOfSpeech: 'noun'
        }).then(data => {
            return data.map(x => {
                return {
                    word: x.word,
                    definition: x.text
                };
            });
        });
    }
    randomWord() {
        let _self = this;
        return _self.fetch(`${_self.wordsUrl}/randomWords`, {
            limit: 1
        }).then(data => data && data.length ? data[0].word : null);
    }
    wordOfTheDay() {
        let _self = this;
        return _self.fetch(`${_self.wordsUrl}/wordOfTheDay`).then(data => data && data.word);
    }
    _relatedWords(word, relationshipType) {
        if (!word) {
            return Promise.reject(`Invalid word - ${word}`);
        }
        let _self = this;
        return _self.fetch(`${_self.wordUrl}/${word}/relatedWords`, {
            relationshipTypes: relationshipType
        }).then(data => data.filter(x => x.relationshipType === relationshipType).map(x => x.words.join(',')).join(','));
    }
    antonym(word) {
        return this._relatedWords(word, 'antonym');
    }
    synonym(word) {
        return this._relatedWords(word, 'synonym');
    }
    exampleUsage(word) {
        let _self = this;
        if (!word) {
            return Promise.reject(`Invalid word - ${word}`);
        }
        return _self.fetch(`${_self.wordUrl}/${word}/topExample`).then(data => data ? data.text : 'No Example Found.');
    }
}

class Service {
    constructor() {

    }
    fetch() {
        return new WordFetchApi();
    }
}

module.exports = Service;