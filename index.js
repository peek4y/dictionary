'use strict';
const program = require('commander');
const Service = require('./service');
const Table = require('cli-table');
const inquirer = require('inquirer');


let service = new Service();

const errorHandler = (err) => {
	console.log('----------Error-----------');
	console.log(err);
};

function fetchEverything(word) {
	return definition(word)
		.then(() => synonym(word))
		.then(() => antonym(word))
		.then(() => sample(word));
}

function definition(word) {
	return service.fetch().definition(word).then(data => {
		let table = new Table({
			head: ['Word', 'Definition'],
			colWidths: [200, 200]
		});
		data.forEach(x => {
			table.push(
				[x.word, x.definition]
			);
		});
		console.log(table.toString());
	});
}

function synonym(word) {
	return service.fetch().synonym(word).then(data => {
		if (!data) {
			console.log('No Synonyms found.');
			return;
		}
		console.log('Word:', word);
		console.log('Synonyms:', data);
	});
}

function antonym(word) {
	return service.fetch().antonym(word).then(data => {
		if (!data) {
			console.log('No Antonyms found..');
			return;
		}
		console.log('Word:', word);
		console.log('Antonyms:', data);
	})
}

function sample(word) {
	return service.fetch().exampleUsage(word).then(data => {
		console.log('Word:', word);
		console.log('Example Usage:', data);
	});
}

function playWordGame() {
	let answer;
	let definition;
	let antonym;
	let synonym;
	console.log('Preparing question. Please wait..');
	return service.fetch().randomWord().then(word => {
		answer = word;
		return service.fetch().definition(word).then(result => {
			if (result && result.length) {
				let randomDef = result[Math.floor(Math.random() * result.length)];
				definition = randomDef && randomDef.definition;
			}
			return service.fetch().synonym(word);
		}).then(syn => {
			let arr = syn.split(',');
			if (arr && arr.length) {
				synonym = arr[Math.floor(Math.random() * arr.length)];
			}
			return service.fetch().antonym(word);
		}).then(ant => {
			let arr = ant.split(',');
			if (arr && arr.length) {
				antonym = arr[Math.floor(Math.random() * arr.length)];
			}
		});
	}).then(() => {
		let questions = [];
		if (definition) {
			questions.push(`Definition: ${definition}`);
		}
		if (synonym) {
			questions.push(`Synonym: ${synonym}`);
		}
		if (antonym) {
			questions.push(`Antonym: ${antonym}`);
		}

		questions.push(`What's the word?`);

		if (questions.length === 1) {
			throw new Error(`Couldn't find a suitable word, try again..`);
		}

		function jumbleWord(word) {
			let letters = word.split('');
			let len = letters.length;

			for (let i = 0; i < len; i++) {
				let j = Math.floor(Math.random() * (i + 1));
				let temp = letters[i];
				letters[i] = letters[j];
				letters[j] = temp;
			}

			return letters.join('');
		}

		function showHint() {
			return inquirer.prompt([{
				type: 'list',
				message: 'Wrong Answer.',
				name: 'wrong',
				choices: [{
					name: 'Try again',
					value: 'retry'
				}, {
					name: 'Show Hint',
					value: 'hint'
				}, {
					name: 'Quit',
					value: 'quit'
				}]
			}]).then(result => {
				let res = result.wrong;
				switch (res) {
					case 'retry': {
						askQuestion();
						break;
					}
					case 'quit': {
						console.log('Nice try..');
						console.log(`Answer is - "${answer}"`);
						break;
					}
					case 'hint': {
						console.log(`Here's a hint..`);
						questions = [];
						questions.push(`Jumbled Word: ${jumbleWord(answer)}`);
						questions.push(`What's the word?`);
						askQuestion();
						break;
					}
					default: {
						console.log('Nice try..');
						console.log(`Answer is - "${answer}"`);
						break;
					}
				}
			});
		}

		function askQuestion() {
			return inquirer.prompt([{
				type: 'input',
				name: 'word',
				message: questions.join('\n')
			}]).then(answers => {
				if (answers.word === answer) {
					console.log(`Ting ting ting.. That's the right answer.`);
					return;
				} else {
					return showHint();
				}
			})
		}

		return askQuestion();
	}).catch(errorHandler);
}

program
	.version('1.0.0');


program
	.command('def [word]')
	.description('fetches definition of a word')
	.action(word => {
		return definition(word).catch(errorHandler);
	});

program
	.command('syn [word]')
	.description('fetches synonym of a word')
	.action(word => {
		return synonym(word).catch(errorHandler);
	});

program
	.command('ant [word]')
	.description('fetches antonyms of a word')
	.action(word => {
		return antonym(word).catch(errorHandler);
	});

program
	.command('ex [word]')
	.description('fetches example for a word')
	.action(word => {
		return sample(word).catch(errorHandler);
	});

program
	.command('play')
	.description('Play word game')
	.action(() => playWordGame());


let allCommand = [program.command('*'), program.command('dict [word]')];

allCommand.map(x => x
	.description('fetches complete details for a word')
	.action(word => {
		return fetchEverything(word)
			.catch(errorHandler);
	})
);

if (process.argv.length <= 2) {
	console.log('Fetching details for the word of the day..');
	return service.fetch().wordOfTheDay().then(word => {
		if (!word) {
			throw new Error(`no word of the day found.`);
		}
		console.log(`Word of the day is - "${word}"`);
		return fetchEverything(word);
	}).catch(errorHandler);
}

program.parse(process.argv);
